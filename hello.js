Groups = new Mongo.Collection("groups");


if (Meteor.isClient)
{
    Meteor.subscribe('groups');


    Session.setDefault('currentGroupId', -1);

    Template.body.helpers({
        groups: function()
        {
            return Groups.find({});
        },
        currentGroup: function()
        {
            var currentId = Session.get('currentGroupId');

            return currentId == -1 ? "No group" : Groups.findOne({_id: currentId}).groupName;
        },
        isInGroup: function()
        {
            return Session.get('currentGroupId') !== -1;
        },
        groupChatContents: function()
        {
            var currentId = Session.get('currentGroupId');
            if(currentId == -1)
            {
                return "not in group";
            }
            else
            {
                return Groups.findOne({_id: currentId}).chatContents.map(function(value)
                {
                    return "Anon: " + value + "\n";
                }).join("");
            }

        }

    })

    Template.body.events({
        'submit #add-new-group': function(e)
        {
            Meteor.call('addGroup', e.target.text.value);

            return false;
        },
        'click #delete-group-button': function(e)
        {
            Meteor.call('removeGroup', Session.get('currentGroupId'));
            Session.set('currentGroupId', -1);
        },
        'submit #chat-input': function(e)
        {
            Meteor.call('updateChat', Session.get('currentGroupId'),  e.target.chat.value)

            e.target.chat.value = "";

            return false;
        }
    });

    Template.group.events({
        'click .group-button': function(e)
        {
            Session.set('currentGroupId', this._id)
        }
    })
}


if (Meteor.isServer)
{
    Meteor.startup(function ()
    {
        // code to run on server at startup
    });

    Meteor.publish('groups', function()
    {
        return Groups.find({});
    });
}



Meteor.methods(
    {
        addGroup: function(groupName)
        {
            Groups.insert({groupName: groupName, chatContents: []})
        },
        removeGroup: function(groupId)
        {
            Groups.remove({_id: groupId});
        },
        updateChat: function(groupId, newText)
        {
            Groups.update({_id: groupId}, {
                $push: {chatContents: newText}
            })
        }

    }
)
